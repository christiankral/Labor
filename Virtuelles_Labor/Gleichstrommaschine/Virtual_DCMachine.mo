within ;
package Virtual_DCMachine
  extends Modelica.Icons.Package;
  model NoLoad "No load experiment"
    extends Modelica.Icons.Example;
    Virtual_DCMachine.Internal.DCEE dcee annotation (Placement(transformation(extent={{0,-50},
              {20,-30}})));
    Modelica.Electrical.Analog.Basic.Ground ground_e
      annotation (Placement(transformation(extent={{-80,-100},{-60,-80}})));
    Modelica.Electrical.Analog.Basic.Ground ground_a annotation (Placement(
          transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={-40,60})));
    Modelica.Electrical.Analog.Sources.ConstantVoltage source_e
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={-70,-40})));
    Modelica.Electrical.Analog.Sources.RampVoltage     source_a(duration=0.4)
      annotation (Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=180,
          origin={10,60})));
    Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor_a
      "Armature current" annotation (Placement(transformation(
          extent={{-10,10},{10,-10}},
          rotation=270,
          origin={30,10})));
    Modelica.Electrical.Analog.Sensors.CurrentSensor currentSensor_e
      "Excitation current"
      annotation (Placement(transformation(extent={{-40,-10},{-20,-30}})));
    Modelica.Mechanics.Rotational.Sensors.SpeedSensor speedSensor annotation (
        Placement(transformation(
          extent={{-10,-10},{10,10}},
          rotation=270,
          origin={40,-70})));
  equation
    connect(ground_e.p, source_e.n)
      annotation (Line(points={{-70,-80},{-70,-50}}, color={0,0,255}));
    connect(ground_e.p, dcee.pin_en) annotation (Line(points={{-70,-80},{-70,-60},
            {-10,-60},{-10,-46},{0,-46}},
                                     color={0,0,255}));
    connect(ground_a.p, source_a.n)
      annotation (Line(points={{-30,60},{0,60}},   color={0,0,255}));
    connect(dcee.pin_an, source_a.n) annotation (Line(points={{4,-30},{4,-10},{-10,
            -10},{-10,60},{0,60}},       color={0,0,255}));
    connect(source_a.p, currentSensor_a.p)
      annotation (Line(points={{20,60},{30,60},{30,20}},color={0,0,255}));
    connect(currentSensor_a.n, dcee.pin_ap) annotation (Line(points={{30,0},{30,-10},
            {16,-10},{16,-30}},      color={0,0,255}));
    connect(source_e.p, currentSensor_e.p)
      annotation (Line(points={{-70,-30},{-70,-20},{-40,-20}},
                                                            color={0,0,255}));
    connect(currentSensor_e.n, dcee.pin_ep) annotation (Line(points={{-20,-20},{-10,
            -20},{-10,-34},{0,-34}},  color={0,0,255}));
    connect(speedSensor.flange, dcee.flange)
      annotation (Line(points={{40,-60},{40,-40},{20,-40}}, color={0,0,0}));
    annotation (Diagram(graphics={
          Rectangle(
            extent={{-20,70},{40,48}},
            lineColor={0,0,0},
            pattern=LinePattern.Dash,
            fillColor={255,213,170},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-20,78},{40,70}},
            lineColor={0,0,0},
            textString="Ankerkreisspannung"),
          Rectangle(
            extent={{-84,-10},{-50,-70}},
            lineColor={0,0,0},
            pattern=LinePattern.Dash,
            fillColor={255,213,170},
            fillPattern=FillPattern.Solid),
          Text(
            extent={{-110,-2},{-50,-10}},
            lineColor={0,0,0},
            textString="Erregerkreisspannung")}), experiment(
              StopTime=2, Tolerance=1e-06, StartTime = 0, Interval = 0.001));
  end NoLoad;

  model Load "Load experiment"
    extends NoLoad;
    Modelica.Mechanics.Rotational.Sources.ConstantTorque constantTorque
      annotation (Placement(transformation(extent={{80,-50},{60,-30}})));
  equation
    connect(constantTorque.flange, dcee.flange)
      annotation (Line(points={{60,-40},{20,-40}}, color={0,0,0}));
    annotation(experiment(StopTime=2, Tolerance=1e-06, StartTime = 0, Interval = 0.001));
  end Load;

  package Internal "Internal Modelica classes"
    extends Modelica.Icons.InternalPackage;
    record LeTTo "LeTTo parameter records"
      extends Modelica.Icons.Record;
      import SI = Modelica.SIunits;
      SI.Inertia Jr[:] = {0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 6.7, 6.7, 6.7, 6.7, 6.7, 6.7, 6.7, 6.7, 6.7, 6.7, 6.7, 6.7, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6, 0.6} "Rotor intertia";
      SI.Resistance Ra[:] = {0.705, 0.705, 0.705, 0.705, 0.705, 0.427, 0.427, 0.427, 0.427, 0.427, 0.803, 0.803, 0.803, 0.803, 0.803, 0.485, 0.485, 0.485, 0.485, 0.485, 0.267, 0.267, 0.267, 0.267, 0.267, 0.162, 0.162, 0.162, 0.162, 0.162, 0.162, 0.162, 0.21, 0.21, 0.21, 0.21, 0.21, 0.21, 0.21, 0.21} "Armature resistance";
      SI.Inductance La[:] = {0.00905, 0.00905, 0.00905, 0.00905, 0.00905, 0.00472, 0.00472, 0.00472, 0.00472, 0.00472, 0.01071, 0.01071, 0.01071, 0.01071, 0.01071, 0.00563, 0.00563, 0.00563, 0.00563, 0.00563, 0.00557, 0.00557, 0.00557, 0.00557, 0.00557, 0.00382, 0.00382, 0.00382, 0.00382, 0.00382, 0.00382, 0.00382, 0.00279, 0.00279, 0.00279, 0.00279, 0.00279, 0.00279, 0.00279, 0.00279} "Armature inductance";
      SI.Voltage VaNominal[:] = {400, 440, 520, 620, 815, 400, 440, 520, 620, 750, 420, 470, 550, 750, 815, 400, 440, 520, 620, 815, 400, 440, 520, 620, 815, 400, 420, 440, 470, 520, 550, 620, 400, 420, 440, 470, 520, 550, 620, 750} "Nominal armature voltage";
      SI.Current IaNominal[:] = {89, 89, 89, 89, 89, 121, 121, 121, 120, 120, 88, 88, 88, 88, 88, 121, 120, 119, 118, 116, 323, 323, 322, 321, 319, 409, 409, 409, 409, 408, 408, 406, 176, 176, 176, 176, 176, 175, 175, 175} "Nominal armature current";
      SI.AngularVelocity wNominal[:] = Modelica.SIunits.Conversions.from_rpm({819, 916, 1113, 1358, 1837, 1071, 1195, 1442, 1752, 2154, 655, 749, 900, 1278, 1400, 810, 906, 1098, 1337, 1649, 208, 235, 288, 355, 485, 285, 302, 319, 355, 388, 413, 474, 1361, 1437, 1512, 1625, 1814, 1927, 2191, 2682}) "Nominal angular velocity";
      SI.Power PmNominal[:] = {29000, 33000, 39000, 47000, 63000, 41000, 46000, 55000, 66000, 81000, 30000, 34000, 41000, 58000, 63000, 40000, 44000, 53000, 64000, 78000, 99000, 112000, 137000, 168000, 228000, 133000, 141000, 149000, 161000, 181000, 193000, 220000, 62000, 65000, 68000, 74000, 82000, 87000, 98000, 120000} "Nominal mechanical power";
      SI.Voltage VeNominal[:] = {400, 300, 375, 200, 250, 250, 200, 375, 300, 400, 250, 200, 320, 320, 400, 400, 320, 320, 200, 250, 350, 400, 250, 400, 500, 500, 350, 400, 250, 400, 500, 350, 200, 200, 250, 250, 500, 500, 400, 400} "Excitation voltage";
      SI.Current IeNominal[:] = {3, 4, 3.2, 6, 4.8, 4.8, 6, 3.2, 4, 3, 6.4, 8, 5, 5, 4, 4, 5, 5, 8, 6.4, 20, 17.5, 28, 17.5, 14, 14, 20, 17.5, 28, 17.5, 14, 20, 8, 8, 6.4, 6.4, 3.2, 3.2, 4, 4} "Excitation current";
      SI.Power PbNominal[:] = {178, 178, 178, 178, 178, 242, 242, 242, 240, 240, 176, 176, 176, 176, 176, 242, 240, 238, 236, 232, 646, 646, 644, 642, 638, 818, 818, 818, 818, 816, 816, 812, 352, 352, 352, 352, 352, 350, 350, 350} "Brush loss";
      SI.Power PfNominal[:] = {838, 398, 1518, 2418, 3773, 906, 746, 1426, 2011, 2611, 566, 966, 1006, 1606, 2326, 1057, 1576, 1774, 2171, 9782, 1698, 1618, 2112, 2866, 4177, 2682, 2862, 3042, 3312, 3377, 3617, 4205, 1543, 2063, 2583, 1863, 2663, 2469, 3719, 4469} "Friction loss @ nominal speed";
    end LeTTo;

    model DCEE "Electrical excited DC machine with LeTTo parameters"
      final parameter Virtual_DCMachine.Internal.LeTTo letto = Virtual_DCMachine.Internal.LeTTo() "LeTTo paramters";
      parameter Integer Nr "LeTTo number Nr";
      extends Modelica.Electrical.Machines.BasicMachines.QuasiStationaryDCMachines.DC_ElectricalExcited(
        final Jr = letto.Jr[Nr],
        final Ra = letto.Ra[Nr],
        final La = letto.La[Nr],
        final VaNominal = letto.VaNominal[Nr],
        final IaNominal = letto.IaNominal[Nr],
        final wNominal = letto.wNominal[Nr],
        final frictionParameters(PRef = letto.PfNominal[Nr], power_w = 1),
        final brushParameters(V = letto.PbNominal[Nr] / letto.IaNominal[Nr], ILinear = 1E-3),
        final IeNominal = letto.IeNominal[Nr],
        final Re = letto.VeNominal[Nr] / letto.IeNominal[Nr],
        final Le = 1,
        final useSupport = false,
        final Js = 0);
    end DCEE;
  end Internal;
  annotation (uses(Modelica(version="3.2.3")));
end Virtual_DCMachine;

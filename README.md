# Virtuelles Labor am TGM

Hier werden virtuelle Laborübung der [Höheren Abteilung für Elektrotechnik am TGM](https://www.tgm.ac.at/tagesschule/elektrotechnik/) angeboten, die auf [OpenMoedlica](https://www.openmodelica.org/) basieren. Die zugehörigen Übungsanleitungen werden auf [LeTTo](https://letto.tgm.ac.at/) zur Verfügung gestellt.
